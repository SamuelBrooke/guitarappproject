﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GuitarTuning : ScriptableObject {

    /// <summary>
    /// Tuning for Guitar contains list of each string tuning
    /// </summary>
    [Header("General Variables")]
    public List<GuitarString> guitarStringList;
}
