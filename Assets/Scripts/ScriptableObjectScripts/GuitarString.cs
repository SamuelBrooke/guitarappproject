﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GuitarString : ScriptableObject {

    /// <summary>
    /// String Tuning, contains string notes list
    /// and corresponding audioClips as a list
    /// </summary>
    [Header("General Variables")]
    public List<string> stringNotes = new List<string>();
    public List<AudioClip> noteSounds = new List<AudioClip>();
}
