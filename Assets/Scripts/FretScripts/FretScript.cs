﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


/// <summary>
/// FretScirpt class defines what is in each fret that is spawned.
/// </summary>
public class FretScript : MonoBehaviour {
    //variables 
    #region Variables
    //[SerializeField] : Makes the variables visible in the engine inspector
    [Header("Basic Variables")]

    [SerializeField] private GameObject fretNoteTextGO;
    [SerializeField] private GameObject fretNumberTextGO;
    [SerializeField] private Text fretNoteText;
    [SerializeField] private Text fretNumberText;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private BoxCollider FretCollider;
    private List<string> notes;

    [SerializeField] private Color transparentColor;
    [SerializeField] private Color visibleColor;
    #endregion

    //Properties
    #region Properties
    public GameObject FretNoteTextGO
    {
        get
        {
            return fretNoteTextGO;
        }
        set
        {
            fretNoteTextGO = value;
        }
    }
    public GameObject FretNumberTextGO
    {
        get
        {
            return fretNumberTextGO;
        }
        set
        {
            fretNumberTextGO = value;
        }
    }
    public Text FretNoteText
    {
        get
        {
            return fretNoteText;
        }
        set
        {
            fretNoteText = value;
        }
    }
    public Text FretNumberText
    {
        get
        {
            return fretNumberText;
        }
        set
        {
            fretNumberText = value;
        }
    }
    public AudioSource FretAudioSource
    {
        get
        {
            return audioSource;
        }
        set
        {
            audioSource = value;
        }
    }
    #endregion

    //testing out using Delegates and events.
    #region Delegates and Events
    public delegate void OnFretPressed(); //delegate
    public event OnFretPressed fretPressedEvent;//instance of delegate
    public delegate void OnFretReleased();
    public event OnFretReleased fretReleasedEvent;
    #endregion
    
    //Initialize on Start
    void Start()
    {
        Debug.Log("text component name" + GetComponentInChildren<Text>().name.ToString());
        fretNoteTextGO.GetComponentInChildren<Text>().color = transparentColor;
    }

    /// <summary>
    /// methods Used to Define functionality
    /// when the frets state changes
    /// </summary>
    #region FretStates Methods
    //Fret Pressed
    public void FretPressed()
    {
        //change Sprite Color and set text visible..
        Debug.Log("Pointer pressed Fret");
        this.GetComponent<SpriteRenderer>().color = Color.red;
        fretNoteTextGO.GetComponentInChildren<Text>().color = visibleColor;
        //Invoking Delegate
        if(fretPressedEvent != null)
        {
            fretPressedEvent();
        }
        //CheckNearByFrets();
        if (audioSource != null)
            audioSource.PlayOneShot(audioSource.clip);
        else
        {
            Debug.Log("audiosource is null");
        }
    }
    //fret released
    public void FretReleased()
    {
        //change Sprite Color and set text transparent..
        Debug.Log("Pointer Released from Fret");
        //Invoking Delegate
        if (fretPressedEvent != null)
        {
            fretPressedEvent();
        }
        this.GetComponent<SpriteRenderer>().color = Color.white;
        fretNoteTextGO.GetComponentInChildren<Text>().color = transparentColor;
    }
    #endregion

    /// <summary>
    /// Changes The Note name of the fretObject.
    /// </summary>
    public void ChangeNoteName(Text changeNoteText, List<string> _noteList, int index)
    {
        if(changeNoteText != null)
        {
            //Vector2Int currentFretCoords;
            //currentFretCoords = this.GetComponent<NoteDeterminationScript>().fretCoords;
            changeNoteText.text = _noteList[index].ToString();
        }
    }

    /// <summary>
    /// Changes The Note name of the fretObject.
    /// </summary>
    public void ChangeFretCountText(Text changeFretText,int index)
    {
        if (changeFretText != null)
        {
            changeFretText.text = index.ToString();
        }

    }

    ///<summary>
    /// Changes The Audioclip of the fretObject(Same note as what the name is set to).
    ///</summary>
    public void ChangeNoteAudioClip(AudioClip audioClip, List<AudioClip> _audioClips, int index)
    {
        if(audioClip == null)
        {
            audioClip = _audioClips[index];
            audioSource.clip = audioClip;
        }
        else
        {
            Debug.Log("AudioClip is already set! value:  " + audioSource.clip.name);
        }

    }

    /// <summary>
    // OLD... new method is in the FretGenerator
    // list of strings that contains the notes.
    // standard tuning starts at lowest 6th string E2
    /// </summary>
    public void PopulateNotesArray(List<string> notesList)
    {
        if(notesList != null)
        {
            var notesToAdd = new[]
            { "E2", "F2", "F#2/Gb2", "G2", "G#2/Ab2",
             "A2", "A#2/Bb2", "B2", "C3", "C#3/Db3",
             "D3","D#3/Eb3" };
            for (int i = 0; i < notesToAdd.Length; i++)
            {
                notesList.Add(notesToAdd[i]);
            }
        }      
    }

    /// <summary>
    // This function can be used for checking
    // for intervals (distance to other notes)
    // based on the current selected note
    /// </summary>
    #region Check for near by frets of current fret.
    public void CheckNearByFrets()
    {
        //Current and neighbour fretGameObjects
        GameObject currentFret;
        GameObject rightFret;
        GameObject leftFret;
        GameObject topFret;
        GameObject bottomFret;

        Vector2Int currentFretPos;
        Vector2Int rightFretPos;
        Vector2Int leftFretPos;
        Vector2Int topFretPos;
        Vector2Int bottomFretPos;

        //set current fret and determine neighbours locations
        currentFret = this.gameObject;
        currentFretPos = currentFret.GetComponent<NoteDeterminationScript>().fretCoords;
        rightFretPos = new Vector2Int(currentFretPos.x + 1, currentFretPos.y);
        leftFretPos = new Vector2Int(currentFretPos.x - 1, currentFretPos.y);
        topFretPos = new Vector2Int(currentFretPos.x, currentFretPos.y + 1);
        bottomFretPos = new Vector2Int(currentFretPos.x, currentFretPos.y - 1);
        List <GameObject> tempList = new List<GameObject>();
        tempList = this.GetComponent<NoteDeterminationScript>().fretList;

        //check nearby frets.
        //ADD later: String amount and fret count to determine if the check is necessary
        if(tempList != null && currentFret != null)
        {
            foreach (GameObject go in tempList)
            {
                //Check and set right fret
                if(go.GetComponent<NoteDeterminationScript>().fretCoords == rightFretPos && currentFret.GetComponent<NoteDeterminationScript>().fretCoords.x < 6)
                {
                    rightFret = go.gameObject;
                    Debug.Log("right fret position" + rightFret.GetComponent<NoteDeterminationScript>().fretCoords);
                    rightFret.GetComponent<FretScript>().fretNoteTextGO.GetComponentInChildren<Text>().text = "A#";
                }
                if(currentFret.GetComponent<NoteDeterminationScript>().fretCoords.x == 6)
                {
                    Debug.Log("No frets to your right!");
                }
                //check and set left fret
                if (go.GetComponent<NoteDeterminationScript>().fretCoords == leftFretPos && currentFret.GetComponent<NoteDeterminationScript>().fretCoords.x > 1)
                {
                    leftFret = go.gameObject;
                    Debug.Log("left fret position" + leftFret.GetComponent<NoteDeterminationScript>().fretCoords);
                    leftFret.GetComponent<FretScript>().fretNoteTextGO.GetComponentInChildren<Text>().text = "B#";
                }
                if (currentFret.GetComponent<NoteDeterminationScript>().fretCoords.x == 1)
                {
                    Debug.Log("No frets to your left!");
                }
                //check and set top fret
                if (go.GetComponent<NoteDeterminationScript>().fretCoords == topFretPos && currentFret.GetComponent<NoteDeterminationScript>().fretCoords.y < 6)
                {
                    topFret = go.gameObject;
                    Debug.Log("top fret position" + topFret.GetComponent<NoteDeterminationScript>().fretCoords);
                    topFret.GetComponent<FretScript>().fretNoteTextGO.GetComponentInChildren<Text>().text = "Gb";
                }
                if (currentFret.GetComponent<NoteDeterminationScript>().fretCoords.y == 6)
                {
                    Debug.Log("No frets on top!");
                }
                //check and set bottom fret
                if (go.GetComponent<NoteDeterminationScript>().fretCoords == bottomFretPos && currentFret.GetComponent<NoteDeterminationScript>().fretCoords.y > 1)
                {
                    bottomFret = go.gameObject;
                    Debug.Log("top fret position" + bottomFret.GetComponent<NoteDeterminationScript>().fretCoords);
                    bottomFret.GetComponent<FretScript>().fretNoteTextGO.GetComponentInChildren<Text>().text = "D";
                }
                if (currentFret.GetComponent<NoteDeterminationScript>().fretCoords.y == 1)
                {
                    Debug.Log("No frets below!");
                }
            }
        }  
    }
    #endregion
}
