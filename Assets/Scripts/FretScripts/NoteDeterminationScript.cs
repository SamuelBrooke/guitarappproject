﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode()]
public class NoteDeterminationScript : MonoBehaviour {

    //these should probably be private with a getter and setter property
    public Vector2Int fretCoords;
    public List<GameObject> fretList;

    /// <summary>
    /// This works,but is not scalable and its too long..
    /// yeah.. talk about overcomplicating things..
    /// commented and replaced with the lower implementation.."AssignFretCoordinates"
    /// </summary>
    #region Check and set location x and y
    //public void CheckFretLocation(GameObject fret, List<GameObject> fretLocs, int guitarStringNum)
    //{
    //    guitarStringNum = guitarStringNum - 1;
    //    foreach(GameObject _fretLoc in fretLocs)
    //    {
    //        //there has got to be an easier way to do this..
    //        //kinda messy code but seems to work for now..
    //        for (int i = 0; i < fretLocs.Count; i++)
    //        {
    //            int x;
    //            int y;
    //            int tempI;

    //            if (i <= 5)
    //            {
    //                tempI = i;
    //                x = 1;
    //                y = 1;
    //                fretCoords = new Vector2Int(x, y + tempI);
    //            }
    //            if (i >= 6 && i < 12)
    //            {
    //                tempI = i;
    //                x = 2;
    //                y = 1;
    //                if (i == 6)
    //                    fretCoords = new Vector2Int(x, y);
    //                else
    //                {
    //                    y =- guitarStringNum;
    //                    fretCoords = new Vector2Int(x, y + tempI);
    //                }   
    //            }
    //            if (i >= 12 && i < 18)
    //            {
    //                tempI = i;
    //                tempI = tempI - (guitarStringNum + 1);
    //                x = 3;
    //                y = 1;
    //                if (i == 12)
    //                {
    //                    fretCoords = new Vector2Int(x, y);
    //                }   
    //                else
    //                {
    //                    y =- guitarStringNum;
    //                    fretCoords = new Vector2Int(x, y + tempI);
    //                }
    //            }
    //            if (i >= 18 && i < 24)
    //            {
    //                tempI = i;
    //                tempI = tempI - (guitarStringNum + 1) - (guitarStringNum + 1);
    //                x = 4;
    //                y = 1;
    //                if (i == 18)
    //                {
    //                    fretCoords = new Vector2Int(x, y);
    //                }
    //                else
    //                {
    //                    y = -guitarStringNum;
    //                    fretCoords = new Vector2Int(x, y + tempI);
    //                }
    //            }
    //            if (i >= 24 && i < 30)
    //            {
    //                tempI = i;
    //                tempI = tempI - (guitarStringNum + 1) - (guitarStringNum + 1) - (guitarStringNum + 1);
    //                x = 5;
    //                y = 1;
    //                if (i == 24)
    //                {
    //                    fretCoords = new Vector2Int(x, y);
    //                }
    //                else
    //                {
    //                    y = -guitarStringNum;
    //                    fretCoords = new Vector2Int(x, y + tempI);
    //                }
    //            }
    //            if (i >= 30 && i < 36)
    //            {
    //                tempI = i;
    //                tempI = tempI - (guitarStringNum + 1) - (guitarStringNum + 1) - (guitarStringNum + 1) - (guitarStringNum + 1);
    //                x = 6;
    //                y = 1;
    //                if (i == 30)
    //                {
    //                    fretCoords = new Vector2Int(x, y);
    //                }
    //                else
    //                {
    //                    y = -guitarStringNum;
    //                    fretCoords = new Vector2Int(x, y + tempI);
    //                }
    //            }

    //        }    
    //    }
    //}
    #endregion

    /// <summary>
    /// New implementation to set fretcoords dynamically
    /// Just simply set the fret coordinate when the notes are generated..
    /// </summary>
    public void AssignFretCoordinates(int guitarStringCount, int fretCount)
    {
        fretCoords = new Vector2Int(fretCount + 1, guitarStringCount + 1);
    }
    //copies content of generated frets to local list
    public void CopyFretList(List<GameObject> fretLocs)
    {
        fretList = new List<GameObject>();
   
        if(fretLocs != null)
        {
            for (int i = 0; i < fretLocs.Count; i++)
            {
                fretList.Add(fretLocs[i]);
            }
        }
    }
}
