﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FretButtonScript : MonoBehaviour {

    public Color defaultFretColor;
    public Color SelectedFretColor;
    private Material mat;
    public GameObject fretObject;
    private FretScript fretScript;


    void Start()
    {
        mat = GetComponent<Renderer>().material;
        fretScript = fretObject.GetComponent<FretScript>();
    }

    public void OnTouchDown()
    {
        //this.GetComponent<SpriteRenderer>().color = Color.red;
        //fretIsPressed = true;
        //mat.color = SelectedFretColor;
        //fretObject.GetComponent<FretScript>().FretPressed();
        if(fretScript != null)
        {
            fretScript.FretPressed();
        }
        if(fretObject != null)
        {
            //set the UI current note to display.
            AppUIScript.instance.CurrentNoteScript.UpdateContentText(fretObject);
        }
        
        //Debug.Log("Pressed fret location: " + fretObject.GetComponent<NoteDeterminationScript>().fretCoords);
    }

    public void OnTouchUp()
    {
        //this.GetComponent<SpriteRenderer>().color = Color.white;
        //fretIsPressed = false;
        //mat.color = defaultFretColor;
        //fretObject.GetComponent<FretScript>().FretReleased();
        if (fretScript != null)
        {
            fretScript.FretReleased();
        }
    }

    public void OnTouchStay()
    {
        //this.GetComponent<SpriteRenderer>().color = Color.green;
        //mat.color = SelectedFretColor;
    }

    public void OnTouchExit()
    {
        //this.GetComponent<SpriteRenderer>().color = Color.white;
        //mat.color = defaultFretColor;
        //fretObject.GetComponent<FretScript>().FretReleased();
        if (fretScript != null)
        {
            fretScript.FretReleased();
        }
    }
}
