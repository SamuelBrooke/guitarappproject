﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(FretBoardGeneratorScript))]
public class FretBoardEditorScript : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        FretBoardGeneratorScript fbEditorScript = (FretBoardGeneratorScript)target;
        if(GUILayout.Button("Generate Fretboard"))
        {
            fbEditorScript.GenerateFretboardGrid();
        }
        if(GUILayout.Button("Clear Fretboard"))
        {
            fbEditorScript.ClearFretboard();
            Debug.Log("Clear the fretboard");
        }
        if (GUILayout.Button("Generate notes"))
        {
            fbEditorScript.GenerateNotes();
            Debug.Log("Generate notes");
        }
        if (GUILayout.Button("Clear notes"))
        {
            fbEditorScript.ClearNotes();
            Debug.Log("Clear notes");
        }
    }

}
#endif