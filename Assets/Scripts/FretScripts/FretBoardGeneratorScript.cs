﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Generates A Fretboard
/// </summary>
[ExecuteInEditMode()]
[Serializable]
public class FretBoardGeneratorScript : MonoBehaviour
{
    //Variables
    #region Variables
    [Header("Basic Variables")]
    public int rows;
    public int columns;
    [SerializeField] private float xOffset = 2.0f; //orig. 1.8f
    [SerializeField] private float yOffset = 2.0f; //orig. 1.6f
    [SerializeField] private float transformScaleX;
    [SerializeField] private float transformScaleY;
    public Vector3 fretboardPos;
    public GameObject fretGameObject;
    public Transform fretBoardTransform;
    public List<GameObject> fretPositions;
    public GuitarTuning currentGuitarTuning;
    #endregion

    private void Awake()
    {
        //setting the application to run at 60 Fps on awake
        Application.targetFrameRate = 60;
    }

    /// <summary>
    /// Generate Fretobject 2dGrid..
    /// </summary>
    #region Generate fret grid
    public void GenerateFretboardGrid()
    {
        fretPositions = new List<GameObject>();

        if (fretGameObject != null)
        {
            for (int x = 0; x < rows; x++)
            {
                for (int y = 0; y < columns; y++)
                {
                    var fret = Instantiate(fretGameObject);
                    fret.transform.position = new Vector3(x * xOffset, y * yOffset, 0.0f);
                    fret.transform.SetParent(this.transform);
                    fretPositions.Add(fret);

                    //set private FretObjects variables on creation.
                    InitializeFretProperties(fret);
                    #region commented Code
                    //overlycomplicated code in CheckFretLocation commented out
                    //fret.GetComponent<NoteDeterminationScript>().CheckFretLocation(fret, fretPositions, columns);
                    #endregion
                    //Y and X are reveresed to generate coordinates correctly for each fret
                    fret.GetComponent<NoteDeterminationScript>().AssignFretCoordinates(y, x);
                    //Debug.Log("Added " + fret.name + "to " + fretboardPos.ToString() + " list");
                    //Debug.Log("Spawned new fret at position: " + fret.transform.position.ToString());
                }
            }
        }
        else
        {
            Debug.Log("No Fret gameobject is set");
        }
        //Set the position of the fretboard after generation
        this.transform.position = fretboardPos;
        this.transform.localScale = new Vector3(transformScaleX, transformScaleY, 1.0f);
        //copy the FretList into each fret notedetermination script
        foreach (GameObject go in fretPositions)
        {
            go.GetComponent<NoteDeterminationScript>().CopyFretList(fretPositions);
        }
        
    }
    #endregion

    //method for intializing fretobject properties in the fret generator
    private void InitializeFretProperties(GameObject go)
    {
        go.GetComponent<FretScript>().FretNoteTextGO = go.transform.Find("NoteTextChild").gameObject;
        go.GetComponent<FretScript>().FretNumberTextGO = go.transform.Find("FretTextParent").gameObject;
        go.GetComponent<FretScript>().FretNoteText = go.GetComponent<FretScript>().FretNoteTextGO.GetComponentInChildren<Text>();
        go.GetComponent<FretScript>().FretNumberText = go.GetComponent<FretScript>().FretNumberTextGO.GetComponentInChildren<Text>();
        go.GetComponent<FretScript>().FretAudioSource = go.GetComponent<AudioSource>();
    }

    #region Clear the fretboard()
    public void ClearFretboard()
    {
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("GuitarFret"))
        {
            if(fooObj != null)
            {
                if (fooObj.name == "GuitarFretParent(Clone)")
                {
                    //Delete the frets
                    DestroyImmediate(fooObj);//works for edit mode
                    fretPositions.Clear();
                }
            }
        }
        //Set the position and scale of the fretboard after the board has been cleared
        this.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
        this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
    #endregion
    
    /// <summary>
    /// Generate Note texts and audioclips to the frets
    /// </summary>
    public void GenerateNotes()
    {
        //New Code
        #region current way of generating the notes to each fret
        Vector2Int _currentFretCoord;
        Text localNoteText;
        AudioSource localAudioSource;
        if (fretPositions != null && currentGuitarTuning != null)
        {
            List<string> _tempNoteList = new List<string>();
            List<AudioClip> _tempAudioClips = new List<AudioClip>();
            foreach (GameObject _fret in fretPositions)
            {
                if(_fret != null)
                {
                    //assign the temporary coordinate from Note determination.
                    _currentFretCoord = new Vector2Int(_fret.GetComponent<NoteDeterminationScript>().fretCoords.x, _fret.GetComponent<NoteDeterminationScript>().fretCoords.y);
                    
                    int _currentString = _currentFretCoord.y;
                    int _stringListIndex = _currentFretCoord.y - 1;
                    int _noteListIndex = _currentFretCoord.x - 1;

                    //deactivate all the fretnumbertext at first.
                    _fret.GetComponent<FretScript>().FretNumberTextGO.SetActive(false);

                    //this is kinda messy..
                    //current guitar is a six string guitar.
                    //checking which string we are currently on and setting the Notes strings and audioclips accordingly.
                    switch (_currentString)
                    {
                        //Currently on the lowest string
                        case 1:
                            _fret.GetComponent<FretScript>().FretNumberTextGO.SetActive(true);
                            localNoteText = _fret.GetComponent<FretScript>().FretNoteText;
                            UpdateFretNotes(localNoteText, _currentString, _fret, _stringListIndex, _noteListIndex, _tempNoteList);
                            localAudioSource = _fret.GetComponent<FretScript>().FretAudioSource;
                            UpdateFretAudio(localAudioSource.clip, _currentString, _fret, _stringListIndex, _noteListIndex, _tempAudioClips);
                            //Making sure we don't get null on the fretNumberText object
                            if(_fret.GetComponent<FretScript>().FretNumberTextGO != null)
                            {
                                UpdateFretNumberText(_fret, _noteListIndex);
                            }
                            
                            break;
                        //Currently on the 2nd lowest string
                        case 2:
                            localNoteText = _fret.GetComponent<FretScript>().FretNoteText;
                            UpdateFretNotes(localNoteText, _currentString, _fret, _stringListIndex, _noteListIndex, _tempNoteList);
                            localAudioSource = _fret.GetComponent<FretScript>().FretAudioSource;
                            UpdateFretAudio(localAudioSource.clip, _currentString, _fret, _stringListIndex, _noteListIndex, _tempAudioClips);
                            break;
                        //Currently on the 3rd lowest string
                        case 3:
                            localNoteText = _fret.GetComponent<FretScript>().FretNoteText;
                            UpdateFretNotes(localNoteText, _currentString, _fret, _stringListIndex, _noteListIndex, _tempNoteList);
                            localAudioSource = _fret.GetComponent<FretScript>().FretAudioSource;
                            UpdateFretAudio(localAudioSource.clip, _currentString, _fret, _stringListIndex, _noteListIndex, _tempAudioClips);
                            break;
                        //Currently on the 3rd Highest string
                        case 4:
                            localNoteText = _fret.GetComponent<FretScript>().FretNoteText;
                            UpdateFretNotes(localNoteText, _currentString, _fret, _stringListIndex, _noteListIndex, _tempNoteList);
                            localAudioSource = _fret.GetComponent<FretScript>().FretAudioSource;
                            UpdateFretAudio(localAudioSource.clip, _currentString, _fret, _stringListIndex, _noteListIndex, _tempAudioClips);
                            break;
                        //Currently on the 2nd Highest string
                        case 5:
                            localNoteText = _fret.GetComponent<FretScript>().FretNoteText;
                            UpdateFretNotes(localNoteText, _currentString, _fret, _stringListIndex, _noteListIndex, _tempNoteList);
                            localAudioSource = _fret.GetComponent<FretScript>().FretAudioSource;
                            UpdateFretAudio(localAudioSource.clip, _currentString, _fret, _stringListIndex, _noteListIndex, _tempAudioClips);
                            break;
                        //Currently on the Highest string
                        case 6:
                            localNoteText = _fret.GetComponent<FretScript>().FretNoteText;
                            UpdateFretNotes(localNoteText, _currentString, _fret, _stringListIndex, _noteListIndex, _tempNoteList);
                            localAudioSource = _fret.GetComponent<FretScript>().FretAudioSource;
                            UpdateFretAudio(localAudioSource.clip, _currentString, _fret, _stringListIndex, _noteListIndex, _tempAudioClips);
                            break;
                        default:
                            break;
                    }
                }
          
            }
        }
        #endregion
    }

    //Update the note names from the current guitarstring ScriptableObject String notelist.
    public void UpdateFretNotes(Text tempNoteText, int guitarString, GameObject fretGO, int guitarStringIndex, int noteListIndex, List<string> tempNoteList)
    {
        //Debug.Log("all fret positions on: " + guitarString + " string: " + fretGO.GetComponent<NoteDeterminationScript>().fretCoords.ToString());
        tempNoteList = currentGuitarTuning.guitarStringList[guitarStringIndex].stringNotes;
        fretGO.GetComponent<FretScript>().ChangeNoteName(tempNoteText, tempNoteList, noteListIndex);
    }

    //Update the note audioclips from the current guitarstring ScriptableObject AudioClip List
    public void UpdateFretAudio(AudioClip audioClip, int guitarString, GameObject fretGO, int guitarStringIndex, int noteListIndex, List<AudioClip> audioClips)
    {
        //Debug.Log("all fret positions on: " + guitarString + " string: " + fretGO.GetComponent<NoteDeterminationScript>().fretCoords.ToString());
        audioClips = currentGuitarTuning.guitarStringList[guitarStringIndex].noteSounds;
        fretGO.GetComponent<FretScript>().ChangeNoteAudioClip(audioClip, audioClips, noteListIndex);
    }

    //Update the fretCount Text
    public void UpdateFretNumberText(GameObject fretGO, int noteListIndex)
    {
        Text tempFretCountText = fretGO.GetComponent<FretScript>().FretNumberText;
        fretGO.GetComponent<FretScript>().ChangeFretCountText(tempFretCountText, noteListIndex);
    }

    #region Clear the notes
    public void ClearNotes()
    {
        if(fretPositions!= null)
        {
            foreach (GameObject goFret in fretPositions)
            {
                if (goFret != null)
                    goFret.GetComponent<FretScript>().FretNoteText.text = "C";
                else
                    throw new System.ArgumentException("no fret gameobjects in fretPositions!");

            }
        }
    }
    #endregion


}
