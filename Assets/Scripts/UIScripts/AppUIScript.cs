﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppUIScript : MonoBehaviour
{
    [SerializeField] private AppUiCurrentNoteScript currentNoteScript;

    //Static instance of the AppUiClass
    public static AppUIScript instance;

    public AppUiCurrentNoteScript CurrentNoteScript
    {
        get
        {
            return currentNoteScript;
        }
    }
    //Initialize on Start
    void Start()
    {
        //needed for making a static instance of this class.
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        currentNoteScript = transform.GetChild(0).gameObject.GetComponent<AppUiCurrentNoteScript>();
    }

}
