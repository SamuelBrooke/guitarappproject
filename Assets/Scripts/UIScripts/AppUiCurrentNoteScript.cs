﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppUiCurrentNoteScript : MonoBehaviour {

    //this might be a temporary script.. just to show the idea.
    //Use this for initialization
    [SerializeField] private Text currentlySelectedNote;
    [SerializeField] private GameObject content;

    //Initialize on Start
	void Start () {
        if(content == null)
        {
            content = this.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
        }
        if(content != null)
        {
            currentlySelectedNote = content.GetComponent<Text>();
        }
        //Debug.Log("content is currently: " + content.name);
    }
	
    public void UpdateContentText(GameObject go)
    {
        currentlySelectedNote = go.GetComponent<FretScript>().FretNoteText;
        Debug.Log("currentNote to update is: " + currentlySelectedNote.text.ToString());
        content.GetComponent<Text>().text = currentlySelectedNote.text;
    }

}
